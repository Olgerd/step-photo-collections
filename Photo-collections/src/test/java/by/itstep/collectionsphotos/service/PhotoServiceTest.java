package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
public class PhotoServiceTest {
    @Autowired
    private DatabaseCleaner dbCleaner;
    @Autowired
    private PhotoService photoService;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;

    @BeforeEach
    public void setUp(){

        dbCleaner.clean();
    }

    @Test
    public void deleteById_happyPath(){
        //given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);
        PhotoEntity photo = EntityUtils.preparePhoto();
        photoRepository.create(photo);

        CollectionEntity collection = EntityUtils.prepareCollection(user);
        collectionRepository.create(collection);
        collection.setPhotos(Arrays.asList(photo));
        collectionRepository.update(collection);

        //when
        photoService.deleteById(photo.getId());

        //then
        Assertions.assertNotNull(userRepository.findById(user.getId()));
        Assertions.assertNotNull(collectionRepository.findById(collection.getId()));
        Assertions.assertNull(photoRepository.findById(photo.getId()));
    }
}
