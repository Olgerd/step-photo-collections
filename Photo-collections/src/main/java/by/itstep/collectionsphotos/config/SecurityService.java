package by.itstep.collectionsphotos.config;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.omg.IOP.ServiceContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity getAuthenticatedUser(){
        String login = SecurityContextHolder.getContext().getAuthentication().getName();

        return userRepository.findByLogin(login);
    }

}
