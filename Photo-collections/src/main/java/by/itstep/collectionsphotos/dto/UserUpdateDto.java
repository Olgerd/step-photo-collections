package by.itstep.collectionsphotos.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer id;

    @NotEmpty(message = "Login can not be blank!")
    @Size(min= 3,max= 20,message = "Login length must be between 3 and 20")
    private String login;

    @NotBlank(message = "Name can ne be blank!")
    private String name;

}
