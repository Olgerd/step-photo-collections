package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.CollectionShortDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CollectionMapper {

    @Autowired
    private UserMapper userMapper;

    public List<CollectionShortDto> map(List<CollectionEntity> entities) {
        List<CollectionShortDto> dtos = new ArrayList<>();
        for (CollectionEntity entity : entities) {
            CollectionShortDto dto = new CollectionShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setDescription(entity.getDescription());

            dtos.add(dto);
        }
        return dtos;
    }


}
