package by.itstep.collectionsphotos.controller;


import by.itstep.collectionsphotos.dto.CollectionShortDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.service.CollectionService;
import by.itstep.collectionsphotos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CollectionController {

    @Autowired
    private CollectionService collectionService;
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.GET)
    public List<CollectionEntity> findAllCollections(){
        List<CollectionEntity> allCollections = collectionService.findAll();
        System.out.println(allCollections);
        return allCollections;
    }
    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.GET)
    public CollectionEntity findById(@PathVariable Integer id){
        CollectionEntity collection = collectionService.findById(id) ;
        return collection;
    }

    @ResponseBody
    @RequestMapping(value = "/users/{userId}/collections",method = RequestMethod.POST)
    public CollectionEntity create(@PathVariable Integer userId, @RequestBody CollectionEntity createRequest){
        CollectionEntity createdCollection = collectionService.create(userId, createRequest);
        return createdCollection;
    }

    @ResponseBody
    @RequestMapping(value = "/collections",method = RequestMethod.PUT)
    public CollectionEntity update(@RequestBody CollectionEntity updateRequest){
      CollectionEntity updatedCollection = collectionService.update(updateRequest);
      return updatedCollection;
    }

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.DELETE)
     public void delete(@PathVariable Integer id){
        collectionService.delete(id);
     }
}
