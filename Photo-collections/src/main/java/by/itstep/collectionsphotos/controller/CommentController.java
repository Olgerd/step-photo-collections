package by.itstep.collectionsphotos.controller;


import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public List<CommentFullDto> findAllComments() {
        List<CommentFullDto> allComments = commentService.findAll();

        System.out.println(allComments);

        return allComments;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.GET)
    public CommentFullDto findById(@PathVariable Integer id) {
        CommentFullDto comment = commentService.findById(id);

        return comment;
    }

    @ResponseBody
    @RequestMapping(value = "/user/{userId}/photos/{photoId}/comments", method = RequestMethod.POST)
    public CommentFullDto create(@PathVariable Integer userId,
                                   @PathVariable Integer photoId,
                                   @RequestBody CommentCreateDto createRequest) {
        CommentFullDto createdComment = commentService.create(createRequest);

        return createdComment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.PUT)
    public CommentFullDto update(@RequestBody CommentUpdateDto updateRequest) {

        CommentFullDto updatedComment = commentService.update(updateRequest);
        return updatedComment;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id) {

        commentService.delete(id);
    }

}
