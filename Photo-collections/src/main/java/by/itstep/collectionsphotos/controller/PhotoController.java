package by.itstep.collectionsphotos.controller;


import by.itstep.collectionsphotos.dto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.GET)
    public List<PhotoShortDto> findAllPhoto(){
       List<PhotoShortDto> allPhotos = photoService.findAll();
        System.out.println(allPhotos);
       return allPhotos;
    }
    @ResponseBody
    @RequestMapping(value = "/photos/{id}",method = RequestMethod.GET)
    public PhotoFullDto findById(@PathVariable Integer id){
        PhotoFullDto photo = photoService.findById(id);

        return photo;
    }
    @ResponseBody
    @RequestMapping(value = "/collections/{collectionId}/photos", method = RequestMethod.POST)
    public PhotoFullDto create(@RequestParam Integer collectionId, @RequestBody PhotoCreateDto createRequest){

        PhotoFullDto createdPhoto = photoService.create(createRequest);
        return createdPhoto;
    }
    @ResponseBody
    @RequestMapping(value = "/photos", method = RequestMethod.PUT)
    public PhotoFullDto update(@RequestBody PhotoUpdateDto updateRequest){
        PhotoFullDto updatedPhoto = photoService.update(updateRequest);
        return updatedPhoto;
    }

    @ResponseBody
    @RequestMapping(value = "/photos/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Integer id){
        photoService.deleteById(id);
    }
}
