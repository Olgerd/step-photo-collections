package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CollectionService {
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;



    public CollectionEntity findById(int id){
        CollectionEntity collection = collectionRepository.findById(id);
        if(collection==null){
            throw new RuntimeException("CollectionService -> Collection not found by id:"+id);
        }
        System.out.println("CollectionService -> Found Collection: "+collection);
        return collection;
    }

    public List<CollectionEntity> findAll(){
        List<CollectionEntity> allCollections = collectionRepository.findAll();
        System.out.println("CollectionService -> Found users: "+ allCollections);
        return allCollections;
    }

    public CollectionEntity create(Integer userId, CollectionEntity collection){
        if(collection.getId()!=null){
            throw new RuntimeException("CollectionService -> Can not create collection with id.");
        }
        if(collection.getUser().getId()==null || userRepository.findById(collection.getUser().getId())==null){
            throw new RuntimeException("CollectionService -> Can not create collection without user or userId.");
        }
        CollectionEntity createdCollection =  collectionRepository.create(collection);
        System.out.println("CollectionService -> Created collection: "+ collection);
        return createdCollection;
    }

    public CollectionEntity update(CollectionEntity collection){
        if(collection.getId()==null){
            throw new RuntimeException("CollectionService -> Can not update collection without id.");
        }
        if(collectionRepository.findById(collection.getId())==null){
            throw new RuntimeException("CollectionService -> Collection is not found by id: "+collection.getId());
        }
        if(collection.getUser().getId()==null || userRepository.findById(collection.getUser().getId())==null){
            throw new RuntimeException("CollectionService -> Can not create collection without user or userId.");
        }
        CollectionEntity updatedCollection = collectionRepository.update(collection);
        System.out.println("CollectionService -> Updated collection: "+ collection);
        return updatedCollection;
    }

    public void delete(int id){
        CollectionEntity collectionToDelete = collectionRepository.findById(id);
        if(collectionToDelete==null){
            throw new RuntimeException("CollectionService -> Collection was not found by id: "+ id+
                    " and can not be deleted.");
        }
        collectionRepository.deleteById(id);
        System.out.println("CollectionService -> Deleted collection: "+collectionToDelete);
    }

    public void addPhoto(int collectionId, int photoId){
        CollectionEntity collectionToUpdate = collectionRepository.findById(collectionId);
        List<PhotoEntity> photosFromCollection = collectionToUpdate.getPhotos();
        photosFromCollection.add(photoRepository.findById(photoId));
        collectionRepository.update(collectionToUpdate);
    }

    public void removePhoto(int collectionId, int photoId){
        CollectionEntity collectionToUpdate = collectionRepository.findById(collectionId);
        List<PhotoEntity> photosFromCollection = collectionToUpdate.getPhotos();
        PhotoEntity photoToDelete = photoRepository.findById(photoId);
        if (!photosFromCollection.contains(photoToDelete)){
            throw new RuntimeException("There is no photo with id: "+ photoId+
                    " in collection with id: "+ collectionId);
        }
        photosFromCollection.remove(photoToDelete);
        collectionRepository.update(collectionToUpdate);
    }





}
