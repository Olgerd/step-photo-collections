package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.PublicKey;
import java.util.List;
@Service
public class CommentService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentMapper mapper;

    public CommentFullDto findById(int id){
        CommentEntity comment = commentRepository.findById(id);
        if(comment==null){
            throw new RuntimeException("CommentService -> Comment not found by id: "+id);
        }
        System.out.println("CommentService -> Found comment: "+comment);
        CommentFullDto dto = mapper.map(comment);

        return dto;
    }

    public List<CommentFullDto> findAll(){
        List<CommentEntity> allComments = commentRepository.findAll();
        System.out.println("CommentService -> Found comments: "+allComments);
        List<CommentFullDto> dtos = mapper.map(allComments);

        return dtos;
    }

    public CommentFullDto create(CommentCreateDto createRequest){

        if(createRequest.getUserId()==null || userRepository.findById(createRequest.getUserId())==null){
            throw new RuntimeException("CommentService -> Can not create comment without user or userId.");
        }
        if(createRequest.getPhotoId()==null || photoRepository.findById(createRequest.getPhotoId())==null){
            throw new RuntimeException("CommentService -> Can not create comment without photo or photoId.");
        }
        UserEntity user = userRepository.findById(createRequest.getUserId());
        PhotoEntity photo = photoRepository.findById(createRequest.getPhotoId());

        CommentEntity entity = mapper.map(createRequest);

        entity.setUser(user);
        entity.setPhoto(photo);

        CommentEntity comment = commentRepository.create(entity);
        System.out.println("CommentService -> Created comment: "+entity);
        CommentFullDto dto = mapper.map(comment);

        return dto;
    }

    public CommentFullDto update(CommentUpdateDto updateRequest){
        if(updateRequest.getId()==null){
            throw new RuntimeException("CommentService -> Can not update entity without id");
        }
        if(commentRepository.findById(updateRequest.getId())==null){
            throw new RuntimeException("CommentService -> Comment not found by id: "+ updateRequest.getId());
        }
    //    if(updateRequest.getUser()!=commentRepository.findById(updateRequest.getId()).getUser()){
    //        throw new RuntimeException("CommentService -> You can not update userId.");
    //    }
    //    if(updateRequest.getPhoto()!=commentRepository.findById(updateRequest.getId()).getPhoto()){
    //        throw new RuntimeException("CommentService -> You can not update photoId.");
    //    }
        CommentEntity updatedComment = commentRepository.findById(updateRequest.getId());
        updatedComment.setMessage(updateRequest.getMessage());
        CommentEntity comment = commentRepository.update(updatedComment);
        CommentFullDto dto = mapper.map(comment);

        System.out.println("CommentService -> Updated comment: "+comment);
        return dto;
    }

    public void delete(int id){
        CommentEntity commentToDelete = commentRepository.findById(id);
        if(commentToDelete==null){
            throw new RuntimeException("CommentService -> Comment was not found by id: "+ id+
                    " and can not be deleted.");
        }
        commentRepository.deleteById(id);
        System.out.println("CommentService -> Deleted comment: "+commentToDelete);
    }

}
